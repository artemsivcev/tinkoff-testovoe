package tinkoff.testovoe.repository

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import tinkoff.testovoe.api.model.NewsMain

@Database(entities = arrayOf(NewsMain::class), version = 1)
abstract class NewsDB : RoomDatabase() {

    abstract fun mNewsDao(): NewsDao

    companion object {
        private var INSTANCE: NewsDB? = null

        fun getInstance(context: Context): NewsDB? {
            if (INSTANCE == null) {
                synchronized(NewsDB::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            NewsDB::class.java, "news.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}