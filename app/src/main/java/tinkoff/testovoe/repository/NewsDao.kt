package tinkoff.testovoe.repository

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import tinkoff.testovoe.api.model.NewsMain

@Dao
interface NewsDao {

    @Query("SELECT * from news")
    fun getAll(): NewsMain

    @Insert(onConflict = REPLACE)
    fun insertAll(allNews: NewsMain)

    @Query("DELETE from news")
    fun deleteAll()

}