package tinkoff.testovoe.repository

import io.reactivex.Observable
import tinkoff.testovoe.api.ApiService
import tinkoff.testovoe.api.model.NewsFull
import tinkoff.testovoe.api.model.NewsMain
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(private val apiService: ApiService) {

    fun getAllNewsFromApi(): Observable<NewsMain> = apiService.getAllNews()
    fun getNewsByIdFromApi(newsId : String): Observable<NewsFull> = apiService.getNewsById(newsId)


}