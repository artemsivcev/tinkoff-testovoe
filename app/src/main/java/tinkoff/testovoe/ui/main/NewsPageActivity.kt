package tinkoff.testovoe.ui.main

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import dagger.android.DaggerActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

import tinkoff.testovoe.R
import tinkoff.testovoe.commons.constants.Constants.EXTRA_NEWS_ID
import javax.inject.Inject

class NewsPageActivity() : DaggerActivity(), Parcelable {

    private val mCompositeDisposable by lazy { CompositeDisposable() }
    @Inject lateinit var mNewsPageViewModel: NewsPageViewModel

    private var mTvTitle: TextView? = null
    private var mTvContent: TextView? = null

    private var mNewsId: String? = ""

    val TAG = "NewsPageActivity"

    constructor(parcel: Parcel) : this() {
        mNewsId = parcel.readString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_page)

        mTvTitle = findViewById(R.id.tvTitle)
        mTvContent = findViewById(R.id.tvContent)

        mNewsId = intent.getStringExtra(EXTRA_NEWS_ID);

        loadNews()

    }

    private fun loadNews() {
        mCompositeDisposable.add(mNewsPageViewModel.showNewsById(mNewsId!!)
                .subscribeBy(
                        onComplete = {},
                        onNext = {
                            updateUI(it.payload.title.text, it.payload.content)
                        },
                        onError = {
                            showError(getString(R.string.error_default))
                            Log.d(TAG, it.message)
                            it.printStackTrace()
                        }
                ))
    }

    fun showError(error:String){
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    private fun updateUI(title: String, content: String) {
        mTvTitle!!.text = title
        mTvContent!!.text = content
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mNewsId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AllNewsActivity> {
        override fun createFromParcel(parcel: Parcel): AllNewsActivity {
            return AllNewsActivity(parcel)
        }

        override fun newArray(size: Int): Array<AllNewsActivity?> {
            return arrayOfNulls(size)
        }
    }
}
