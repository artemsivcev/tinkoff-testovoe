package tinkoff.testovoe.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.*
import android.support.v7.widget.RecyclerView
import com.facebook.drawee.backends.pipeline.Fresco
import dagger.android.DaggerActivity
import io.reactivex.disposables.CompositeDisposable

import tinkoff.testovoe.api.model.NewsShort
import tinkoff.testovoe.ui.main.adapter.DelegateAdapter
import tinkoff.testovoe.ui.main.adapter.RecyclerViewAdapter
import java.util.ArrayList
import javax.inject.Inject
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.widget.Toast
import io.reactivex.rxkotlin.subscribeBy
import tinkoff.testovoe.R
import tinkoff.testovoe.api.model.Suggestion
import tinkoff.testovoe.commons.constants.Constants.EXTRA_NEWS_ID
import tinkoff.testovoe.commons.extensions.androidLazy
import tinkoff.testovoe.repository.NewsDB
import tinkoff.testovoe.util.DbWorkerThread
import android.support.v4.widget.SwipeRefreshLayout
import tinkoff.testovoe.api.model.NewsMain
import tinkoff.testovoe.ui.main.listener.ScrollListener
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


class AllNewsActivity() : DaggerActivity(), Parcelable, DelegateAdapter.onViewSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    private val mCompositeDisposable by lazy { CompositeDisposable() }
    private val mRecyclerViewAdapter by androidLazy { RecyclerViewAdapter(this) }

    @Inject lateinit var mAllNewsViewModel: AllNewsViewModel

    private var mRecyclerView: RecyclerView? = null
    private var mSwipeContainer: SwipeRefreshLayout? = null
    private val mTinkoffNews = ArrayList<NewsShort>()

    private var mDb: NewsDB? = null

    private lateinit var mDbWorkerThread: DbWorkerThread
    private val mUiHandler = Handler()

    private val TAG = "AllNewsActivity"


    constructor(parcel: Parcel) : this() {
        //nothing here
    }

    /**
     * simple app to load and store TinkoffNews.
     * Using Kotlin, Rx, Retrofit, Dagger, Room
     * Download and parsing news from api.
     * Sorting and storing it in DB with Room.
     * And we have pull-to-refresh!
     * Any news you can open. If it is a big one, just scroll it.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_news)

        Fresco.initialize(this)

        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()

        mDb = NewsDB.getInstance(this)

        mRecyclerView = findViewById(R.id.recyclerView)
        mRecyclerView?.apply {
            setHasFixedSize(true)
            val gridLayout = GridLayoutManager(context, 1)
            layoutManager = gridLayout
            clearOnScrollListeners()
            addOnScrollListener(ScrollListener(
                    {
                        loadAllNews()
                    }, gridLayout))
        }
        mRecyclerView?.adapter = mRecyclerViewAdapter

        mSwipeContainer = findViewById(R.id.swipeContainer)
        mSwipeContainer?.setOnRefreshListener(this)

        if(savedInstanceState == null){

            fillNewsList()

        }

    }

    // SwipeRefreshLayout stuff

    override fun onRefresh() {
        loadAllNews()
    }

    // RecyclerView stuff
    override fun onItemSelected(news: NewsShort?) {
        val intent = Intent(this, NewsPageActivity::class.java)
        intent.putExtra(EXTRA_NEWS_ID, news?.newsId)
        startActivity(intent)
    }

    private fun loadAllNews() {
        mCompositeDisposable.add(mAllNewsViewModel.showAllNews()
                .subscribeBy(
                        onComplete = {},
                        onNext = {
                            if(it.payload?.size == 0){
                                Log.d(TAG, "ERR!!!")
                                showError(getString(R.string.error_nothing_to_show))
                                return@subscribeBy
                            }
                            insertAllNewsDB(it)
                            updateItemsInList(it.payload!!)
                        },
                        onError = {
                            showError(getString(R.string.error_default))
                            Log.d(TAG, it.message)
                        }
                ))
    }

    fun showError(error:String){
        mUiHandler.post {
            Toast.makeText(this, error, Toast.LENGTH_LONG).show()
        }
    }

    /**
     * update view with news. sorting news here
     */
    private fun updateItemsInList(newsAll: List<NewsShort>) {
        resetStates()
        mTinkoffNews.addAll(newsAll)
        //why you need a magic lambda sort when you response already sorted???!!!
        val sorted =  mTinkoffNews.sortedWith(compareByDescending({ it.publicationDate.milliseconds.toLong() }))
        mRecyclerView?.adapter?.notifyDataSetChanged()
        mRecyclerViewAdapter.clearAndAddNewItems(sorted)
        mSwipeContainer?.setRefreshing(false)
    }

    private fun resetStates() {
        mTinkoffNews.clear()
    }

    //Working with room here

    /**
     * fill new from DB. If its empty, try to load from server
     */
    fun fillNewsList(){
        val task = Runnable {
            val mNewsMain = mDb?.mNewsDao()?.getAll()
            if(mNewsMain != null) {
                mUiHandler.post{updateItemsInList(mNewsMain.payload!!)}
            }else {
                loadAllNews()
            }
        }
        mDbWorkerThread.postTask(task)
    }

    /**
     * insert all new news in DB. Clear if before.
     */
    fun insertAllNewsDB(allNews:NewsMain){
        deleteAllNewsDB()
        val task = Runnable { mDb?.mNewsDao()?.insertAll(allNews) }
        mDbWorkerThread.postTask(task)
    }

    /**
     * just clear DB
     */
    fun deleteAllNewsDB(){
        val task = Runnable { mDb?.mNewsDao()?.deleteAll()}
        mDbWorkerThread.postTask(task)
    }

    //Activity stuff

    /**
     * clear stuff when activity die
     */
    override fun onDestroy() {
        super.onDestroy()
        mDbWorkerThread.quit()
        mCompositeDisposable.clear()
        mCompositeDisposable.dispose()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        //nothing to do
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AllNewsActivity> {
        override fun createFromParcel(parcel: Parcel): AllNewsActivity {
            return AllNewsActivity(parcel)
        }

        override fun newArray(size: Int): Array<AllNewsActivity?> {
            return arrayOfNulls(size)
        }
    }



}
