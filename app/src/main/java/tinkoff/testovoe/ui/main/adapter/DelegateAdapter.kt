package tinkoff.testovoe.ui.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import kotlinx.android.synthetic.main.news_entry.view.*
import tinkoff.testovoe.R
import tinkoff.testovoe.api.model.NewsShort
import tinkoff.testovoe.commons.adapter.ViewType
import tinkoff.testovoe.commons.adapter.ViewTypeDelegateAdapter
import tinkoff.testovoe.commons.extensions.inflate

class DelegateAdapter(val viewActions: onViewSelectedListener) : ViewTypeDelegateAdapter {

    interface onViewSelectedListener {
        fun onItemSelected(newsShort: NewsShort?)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as ViewHolder
        holder.bind(item as NewsShort)
    }

    inner class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.news_entry)) {

        private val tvTitle = itemView.tvTitle

        fun bind(item: NewsShort) {
            tvTitle.text = item.text

            super.itemView.setOnClickListener { viewActions.onItemSelected(item)}
        }
    }

}