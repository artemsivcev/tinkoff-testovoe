package tinkoff.testovoe.ui.main.di

import dagger.Module
import dagger.Provides
import tinkoff.testovoe.repository.Repository
import tinkoff.testovoe.ui.main.NewsPageViewModel
import tinkoff.testovoe.util.SchedulerProvider

@Module
class NewsPageModule {

    @Provides
    fun provideViewModel(repository: Repository, schedulerProvider: SchedulerProvider) = NewsPageViewModel(repository, schedulerProvider)
}