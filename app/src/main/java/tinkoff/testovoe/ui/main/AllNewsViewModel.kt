package tinkoff.testovoe.ui.main

import io.reactivex.Observable
import tinkoff.testovoe.api.model.NewsMain
import tinkoff.testovoe.repository.Repository
import tinkoff.testovoe.util.SchedulerProvider

class AllNewsViewModel(private val repository: Repository, private val schedulerProvider: SchedulerProvider) {

    fun showAllNews(): Observable<NewsMain> = repository.getAllNewsFromApi()
            .compose(schedulerProvider.getSchedulersForObservable<NewsMain>())
}