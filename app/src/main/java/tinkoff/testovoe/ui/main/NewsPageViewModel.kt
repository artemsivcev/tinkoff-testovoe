package tinkoff.testovoe.ui.main

import io.reactivex.Observable
import tinkoff.testovoe.api.model.NewsFull
import tinkoff.testovoe.repository.Repository
import tinkoff.testovoe.util.SchedulerProvider

class NewsPageViewModel(private val repository: Repository, private val schedulerProvider: SchedulerProvider) {

    fun showNewsById(newsId : String): Observable<NewsFull> = repository.getNewsByIdFromApi(newsId)
            .compose(schedulerProvider.getSchedulersForObservable<NewsFull>())

}