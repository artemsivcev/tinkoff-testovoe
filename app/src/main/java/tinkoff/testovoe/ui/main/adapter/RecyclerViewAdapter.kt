package tinkoff.testovoe.ui.main.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tinkoff.testovoe.api.model.NewsShort
import tinkoff.testovoe.commons.adapter.ViewType
import tinkoff.testovoe.commons.adapter.ViewTypeDelegateAdapter
import tinkoff.testovoe.commons.constants.AdapterConstants
import java.util.ArrayList


class RecyclerViewAdapter(listener: DelegateAdapter.onViewSelectedListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: ArrayList<ViewType>
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()
    private val loadingItem = object : ViewType {
        override fun getViewType() = AdapterConstants.LOADING
    }

    init {
        delegateAdapters.put(AdapterConstants.LOADING, LoadingDelegateAdapter())
        delegateAdapters.put(AdapterConstants.NEWS, DelegateAdapter(listener))
        items = ArrayList()
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            delegateAdapters.get(viewType).onCreateViewHolder(parent)


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items[position])
    }

    override fun getItemViewType(position: Int) = items[position].getViewType()

    fun addItems(news: List<NewsShort>) {
        // first remove loading and notify
        val initPosition = items.size - 1
        items.removeAt(initPosition)
        notifyItemRemoved(initPosition)

        // insert news and the loading at the end of the list
        items.addAll(news)
        items.add(loadingItem)
        notifyItemRangeChanged(initPosition, items.size + 1 /* plus loading item */)
    }

    fun clearAndAddNewItems(news: List<NewsShort>) {
        items.clear()
        notifyItemRangeRemoved(0, getLastPosition())

        items.addAll(news)
        //didnt need load more items
//        items.add(loadingItem)
        notifyItemRangeInserted(0, items.size)
    }

    fun clear() {
        items.clear()
        notifyItemRangeRemoved(0, getLastPosition())
    }

    fun getItems(): List<NewsShort> =
            items
                    .filter { it.getViewType() == AdapterConstants.NEWS }
                    .map { it as NewsShort }


    private fun getLastPosition() = if (items.lastIndex == -1) 0 else items.lastIndex

}