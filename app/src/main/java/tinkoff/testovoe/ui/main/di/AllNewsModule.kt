package tinkoff.testovoe.ui.main.di

import dagger.Module
import dagger.Provides
import tinkoff.testovoe.repository.Repository
import tinkoff.testovoe.ui.main.AllNewsViewModel
import tinkoff.testovoe.util.SchedulerProvider

@Module
class AllNewsModule {

    @Provides
    fun provideViewModel(repository: Repository, schedulerProvider: SchedulerProvider) = AllNewsViewModel(repository, schedulerProvider)
}