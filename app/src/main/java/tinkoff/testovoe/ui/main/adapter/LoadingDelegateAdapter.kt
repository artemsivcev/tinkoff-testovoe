package tinkoff.testovoe.ui.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tinkoff.testovoe.R
import tinkoff.testovoe.commons.adapter.ViewType
import tinkoff.testovoe.commons.adapter.ViewTypeDelegateAdapter
import tinkoff.testovoe.commons.extensions.inflate

class LoadingDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup) = LoadingViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
    }

    class LoadingViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.item_loading))
}