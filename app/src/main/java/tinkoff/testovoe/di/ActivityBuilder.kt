package tinkoff.testovoe.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import tinkoff.testovoe.ui.main.NewsPageActivity
import tinkoff.testovoe.ui.main.AllNewsActivity
import tinkoff.testovoe.ui.main.di.AllNewsModule
import tinkoff.testovoe.ui.main.di.NewsPageModule

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(AllNewsModule::class))
    abstract fun bindAllNewsModule(): AllNewsActivity

    @ContributesAndroidInjector(modules = arrayOf(NewsPageModule::class))
    abstract fun bindNewsPageActivity(): NewsPageActivity
}