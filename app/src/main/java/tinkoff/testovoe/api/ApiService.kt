package tinkoff.testovoe.api

import retrofit2.http.GET
import retrofit2.http.Query
import io.reactivex.Observable
import tinkoff.testovoe.api.model.NewsFull
import tinkoff.testovoe.api.model.NewsMain
import tinkoff.testovoe.commons.constants.Constants.API_VERSION

interface ApiService {

    @GET(API_VERSION+"news")
    fun getAllNews(): Observable<NewsMain>

    @GET(API_VERSION+"news_content?")
    fun getNewsById(@Query("id") newsId: String): Observable<NewsFull>

}