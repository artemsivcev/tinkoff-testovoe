package tinkoff.testovoe.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
data class NewsFull(
        @SerializedName("resultCode") val resultCode: String,
        @SerializedName("payload") val payload: TinkoffPayloadNews,
        @SerializedName("trackingId") val perpage: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class TinkoffPayloadNews(
        @SerializedName("title") val title: NewsShort,
        @SerializedName("content") val content: String
)

