package tinkoff.testovoe.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName
import tinkoff.testovoe.commons.adapter.ViewType
import tinkoff.testovoe.commons.constants.AdapterConstants

@JsonIgnoreProperties(ignoreUnknown = true)
data class NewsShort(
        @SerializedName("id") val newsId: String,
        @SerializedName("name") val name: String,
        @SerializedName("text") val text: String,
        @SerializedName("publicationDate") val publicationDate: PublicationDate
) : ViewType {

    override fun getViewType() = AdapterConstants.NEWS

}

@JsonIgnoreProperties(ignoreUnknown = true)
data class PublicationDate(
        @SerializedName("milliseconds") val milliseconds: String
)