package tinkoff.testovoe.api.model

import android.arch.persistence.room.*
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName
import tinkoff.testovoe.commons.extensions.RoomListConverter

@Entity(tableName = "news")
@TypeConverters(RoomListConverter::class)
@JsonIgnoreProperties(ignoreUnknown = true)
data class NewsMain(
        @PrimaryKey(autoGenerate = true) var _id: Long?,
        @ColumnInfo(name = "resultCode") @SerializedName("resultCode") var resultCode: String,
        @ColumnInfo(name = "payload") @SerializedName("payload") var payload: List<NewsShort>?,
        @ColumnInfo(name = "trackingId") @SerializedName("trackingId") var trackingId: String
){
    constructor():this(null,"", null ,"")
}