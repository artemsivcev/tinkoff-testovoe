package tinkoff.testovoe.commons.extensions

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tinkoff.testovoe.api.model.NewsShort
import java.util.*


class RoomListConverter {

    var gson = Gson()

    @TypeConverter
    fun stringToNewsShortList(data: String?): List<NewsShort> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<NewsShort>>() {

        }.getType()

        return gson.fromJson<List<NewsShort>>(data, listType)
    }

    @TypeConverter
    fun newsShortListToString(someObjects: List<NewsShort>): String {
        return gson.toJson(someObjects)
    }

}