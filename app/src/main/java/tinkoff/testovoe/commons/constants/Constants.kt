package tinkoff.testovoe.commons.constants

object Constants {

    const val EXTRA_NEWS_ID = "url"

    const val TINKOFF_DOMAIN = "https://api.tinkoff.ru"
    const val API_VERSION = "v1/"

}