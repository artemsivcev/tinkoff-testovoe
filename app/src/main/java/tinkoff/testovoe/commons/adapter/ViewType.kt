package tinkoff.testovoe.commons.adapter

interface ViewType {
    fun getViewType(): Int
}